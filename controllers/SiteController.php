<?php

namespace app\controllers;

use app\models\Project;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

	public function behaviors() {
		return [];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		$data = Project::getAll(12, 'id desc');

		return $this->render('index',[
			'projects'=>$data['projects'],
		]);
	}

	public function actionProjects($id = null) {
		$data = Project::getAll(12, 'id desc');
		$recent = Project::getRecent();

		return $this->render('projects',[
			'projects'=>$data['projects'],
			'pagination'=>$data['pagination'],
			'recent'=>$recent,
		]);
	}

	public function actionView($id) {

		$project = Project::findOne($id);
		$recent = Project::getRecent();

		$project->viewedCounter();
		return $this->render('single',[
			'project'=>$project,
			'recent'=>$recent,
			'categories'=>$categories,
		]);
	}

}
