<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170124_021553_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
            'description'=>$this->text(),
            'content'=>$this->text(),
            'date'=>$this->date(),
            'price'=>$this->integer(),
            'image'=>$this->string(),
            'viewed'=>$this->integer(),
            'contact'=>$this->text(),
            'user_id'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
