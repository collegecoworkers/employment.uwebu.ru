<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Category;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <!-- Bootstrap core CSS -->
  <link href="/public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="/public/css/modern-business.css" rel="stylesheet">

</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('/partials/header');?>

<?= $content ?>

<?= $this->render('/partials/footer');?>

<!-- Bootstrap core JavaScript -->
<script src="/public/vendor/jquery/jquery.min.js"></script>
<script src="/public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
