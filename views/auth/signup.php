<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<br>
<br>
<br>
<br>
<br>
<br>
<!-- banner -->
<div class="inside-banner">
  <div class="container text-center"> 
    <h2><?= $this->title ?></h2>
  </div>
</div>
<!-- banner -->

<div class="container">
  <div class="spacer">
    <div class="row justify-content-md-center">
      <div class="col-lg-6 col-lg-auto col-sm-6 col-sm-offset-3 col-xs-12 ">
				<?php 
					$form = ActiveForm::begin([
						'id' => 'login-form',
					]);
				?>

				<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

				<?= $form->field($model, 'email')->textInput() ?>

				<?= $form->field($model, 'password')->passwordInput() ?>

						<?= Html::submitButton('Отправить', ['class' => 'btn btn-success' ]) ?>

				<?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<br>
<br>
<br>
