<!-- CONTENT BEGIN -->
<div id="content" class="container">
    <div class="inner">
        <div class="block_404">
            <h1>404</h1>
            <h2>We are sorry, but the page you are looking for can not be found.</h2>
            <p>You can try searching our site or visit the homepage.</p>
        </div>
    </div>
</div>
<!-- CONTENT END -->