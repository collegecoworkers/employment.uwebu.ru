<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<!-- Page Content -->
<div class="container">

	<!-- Page Heading/Breadcrumbs -->
	<h1 class="mt-4 mb-3">Все вакансии
	</h1>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/">Главная</a>
		</li>
		<li class="breadcrumb-item active">Все вакансии</li>
	</ol>

		<div class="row">
	<?php foreach($projects as $item):?>
		<!-- Blog Post -->
			<div class="col-lg-6 portfolio-item">
				<div class="card h-100">
					<a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><img class="card-img-top" src="<?= $item->getImage() ?>" alt=""></a>
					<div class="card-body">
						<h4 class="card-title">
							<a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a>
						</h4>
						<p class="card-text"><?= $item->description ?></p>
					</div>
				</div>
			</div>
	<?php endforeach ?>
		</div>

	<ul class="pagination justify-content-center">
		<?php
		echo LinkPager::widget([
			'pagination' => $pagination,
		]);
		?>
	</ul>

</div>