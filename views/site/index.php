<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
        <ol class="carousel-indicators">

          <?php for ($i=0; $i < count($projects) && $i < 5; $i++) { 
            echo '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class="'.(($i == 0) ? "active" : "") .'"></li>';
          } ?>
        </ol>
        <div class="carousel-inner" role="listbox">
          <?php for ($i=0; $i < count($projects) && $i < 5; $i++): $item = $projects[$i]; ?>
            <div class="carousel-item <?= (($i == 0) ? "active" : "") ?>" style="background-image: url('<?= $item->getImage() ?>')">
              <div class="carousel-caption d-none d-md-block">
                <h3><?= $item->title ?></h3>
                  <p class="card-text"><?= $item->description ?></p>
                </div>
              </div>
            <?php endfor;?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>

<div class="container">

  <h1 class="my-4">Новые вакансии</h1>

  <div class="row">
    <?php foreach($projects as $item):?>
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-200">
          <a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><img class="card-img-top" src="<?= $item->getImage() ?>" alt="" style="height:200px"></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a>
            </h4>
            <p class="card-text">Зарплата: <?= $item->price ?></p>
          </div>
        </div>
      </div>
    <?php endforeach;?>
  </div>
  <!-- /.row -->

</div>