<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\User;

?>
<!-- Page Content -->
<div class="container">

	<!-- Page Heading/Breadcrumbs -->
	<h1 class="mt-4 mb-3"><?= $project->title ?></h1>

	<div class="row">
		<div class="col-md-4">

			<div class="card my-4">
				<h5 class="card-header">Присылайте резюме сюда</h5>
				<div class="card-body">
					<?= $project->contact ?>
				</div>
			</div>


			<?= $this->render('/partials/sidebar', ['projects'=>$recent,]);?>
		</div>
		<!-- Post Content Column -->
		<div class="col-lg-8">

			<!-- Preview Image -->
			<img class="img-fluid rounded" src="<?= $project->getImage();?>" alt="">

			<hr>

			<p>Автор: <?= User::findIdentity($project->user_id)->name ?></p>

			<hr>

			<!-- Post Content -->
			<p class="lead">
				<?= $project->content ?>
			</p>

		</div>
	</div>
	<!-- /.row -->

</div>
