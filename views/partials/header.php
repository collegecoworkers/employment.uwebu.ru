<?php
use yii\helpers\Url;
?>
<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="/">Система трудоустройства</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/']) ?>">Главная</a></li>
        <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/site/projects']) ?>">Вакансии</a></li>
        <?php if(Yii::$app->user->isGuest):?>
          <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/auth/login'])?>">Вход</a></li>
          <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/auth/signup'])?>">Регистрация</a></li>
        <?php else: ?>
          <?php if(Yii::$app->user->identity->isAdmin && false): ?>
            <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/admin'])?>">Админка</a></li>
          <?php endif ?>
          <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/user/my-projects'])?>">Мои вакансии</a></li>
          <li  class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['/auth/logout'])?>">Выход <?= ' - ' . Yii::$app->user->identity->name ?></a></li>
        <?php endif;?>
      </ul>
    </div>
  </div>
</nav>