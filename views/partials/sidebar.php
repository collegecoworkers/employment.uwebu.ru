<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="card my-4">
	<h5 class="card-header">Новые предложения</h5>
	<div class="card-body">
		<div class="row">
			<div class="col-lg-12">
				<ul class="list-unstyled mb-0">
					<?php foreach($projects as $item):?>
					<li>
						<a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a>
					</li>
					<?php endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</div>