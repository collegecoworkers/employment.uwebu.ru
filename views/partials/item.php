<?php
use yii\helpers\Url;
?>
<?php foreach($projects as $project):?>
	<article class="post_type_4">
		<div class="feature">
			<div class="image">
				<a href="<?= Url::toRoute(['site/view', 'id'=>$project->id]);?>">
					<img class="item-img" src="<?= $project->getImage();?>" alt=""><span class="hover"></span>
				</a>
			</div>
		</div>
		
		<div class="content">
			<div class="info">
				<div class="date"><?= $project->getDate();?></div>
				<div class="stats">
					<div class="views"><?= (int)$project->viewed ?></div>
				</div>
			</div>
			
			<div class="title">
				<a href="<?= Url::toRoute(['site/view', 'id'=>$project->id]);?>"><?= $project->title ?></a>
			</div>
			
			<div class="line_1"></div>
		</div>
	</article>
<?php endforeach; ?>