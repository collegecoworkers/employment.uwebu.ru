<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Вакансии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="block_general_title_1">
	<h1><?= $this->title ?></h1>
</div>
<div id="content" class="sidebar_right">
	<div class="inner">
	
		<div class="article-view">

			<p>
				<?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
				<?= Html::a('Добавить изтбражение', ['set-image', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
				<?= Html::a('удалить', ['delete', 'id' => $model->id], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => 'Вы уверены?',
						'method' => 'post',
					],
				]) ?>
			</p>

			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'id',
					'title',
					'description:ntext',
					'content:ntext',
					'date',
					'price',
					'image',
					'viewed',
					'contact',
				],
			]) ?>

		</div>
	</div>
</div>
</div>
