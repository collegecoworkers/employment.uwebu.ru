<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = 'Изменить вакансию: ' . $model->title;
?>
<div class="container">
<div class="block_general_title_1">
	<h1><?= $this->title ?></h1>
</div>
<div id="content" class="sidebar_right">
	<div class="inner">

		<div class="article-update">

			<?= $this->render('_form', [
				'model' => $model,
				]) ?>

		</div>
	</div>
	</div>
</div>
