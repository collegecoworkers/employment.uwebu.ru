<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container">
<div class="block_general_title_1">
	<h1><?= $this->title ?></h1>
</div>
<div id="content" class="sidebar_right" style="height: calc(100vh - 280px);">
	<div class="inner">
		<div class="article-form">
			<?php $form = ActiveForm::begin(); ?>
			<?= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>
			<div class="form-group">
				<?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
</div>
